"""General-purpose test script for image-to-image translation.

Once you have trained your model with train.py, you can use this script to test the model.
It will load a saved model from --checkpoints_dir and save the results to --results_dir.

It first creates model and dataset given the option. It will hard-code some parameters.
It then runs inference for --num_test images and save results to an HTML file.

Example (You need to train models first or download pre-trained models from our website):
    Test a CycleGAN model (both sides):
        python test.py --dataroot ./datasets/maps --name maps_cyclegan --model cycle_gan

    Test a CycleGAN model (one side only):
        python test.py --dataroot datasets/horse2zebra/testA --name horse2zebra_pretrained --model test --no_dropout

    The option '--model test' is used for generating CycleGAN results only for one side.
    This option will automatically set '--dataset_mode single', which only loads the images from one set.
    On the contrary, using '--model cycle_gan' requires loading and generating results in both directions,
    which is sometimes unnecessary. The results will be saved at ./results/.
    Use '--results_dir <directory_path_to_save_result>' to specify the results directory.

    Test a pix2pix model:
        python test.py --dataroot ./datasets/facades --name facades_pix2pix --model pix2pix --direction BtoA

See options/base_options.py and options/test_options.py for more test options.
See training and test tips at: https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/blob/master/docs/tips.md
See frequently asked questions at: https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/blob/master/docs/qa.md
"""
import os
import cv2
import numpy as np
from options.test_options import TestOptions
from data import create_dataset
from models import create_model
from util.visualizer import save_images
from util import html
import util.util as util


if __name__ == '__main__':
    opt = TestOptions().parse()  # get test options
    opt.num_threads = 0   # test code only supports num_threads = 1
    opt.batch_size = 1    # test code only supports batch_size = 1
    opt.serial_batches = True  # disable data shuffling; comment this line if results on randomly chosen images are needed.
    opt.no_flip = True    # no flip; comment this line if results on flipped images are needed.
    opt.display_id = -1   # no visdom display; the test code saves the results to a HTML file.
    #opt.n_downsampling = 4 # TODO you have to enter it manually based on your training settings
    opt.netG = "resnet_9blocks"
    opt.load_size = opt.load_size_test = 128
    opt.phase = "eval"
    opt.epoch = opt.epoch_test[0]
    opt.checkpoints_dir = opt.checkpoints_dir_test[0]
    opt.netD = 'n_layers'
    opt.n_layers_D = 2
    opt.gan_mode ='lsgan'
    # for i, dirs in enumerate(self.opt.checkpoints_dir_test[0].split(',')):
    #         self.opt.checkpoints_dir = dirs.strip()
    #         self.opt.epoch = self.opt.epoch_test[0].split(',')[i]
    dataset = create_dataset(opt)  # create a dataset given opt.dataset_mode and other options
    model = create_model(opt)      # create a model given opt.model and other options
    

    for i, data in enumerate(dataset):
        if i == 0:
            model.data_dependent_initialize(data)
            model.setup(opt)               # regular setup: load and print networks; create schedulers
            model.parallelize()
            if opt.eval:
                model.eval()
        if i >= opt.num_test:  # only apply our model to opt.num_test images.
            break
        model.set_input(data)  # unpack data from data loader
        model.test()           # run inference
        visuals = model.get_current_visuals()  # get image results
        img_path = model.get_image_paths()     # get image paths
        
        infered = visuals['fake_B'].detach().cpu().numpy().squeeze()
        infered = cv2.normalize(infered, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)
        infered = np.moveaxis(infered, 0, -1)
        infered =cv2.cvtColor(infered, cv2.COLOR_BGR2RGB)
        
        syn = visuals['real_A'].detach().cpu().numpy().squeeze()
        syn = cv2.normalize(syn, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)
        syn = np.moveaxis(syn, 0, -1)
        syn =cv2.cvtColor(syn, cv2.COLOR_BGR2RGB)
        
        real = visuals['real_B'].detach().cpu().numpy().squeeze()
        real = cv2.normalize(real, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)
        real = np.moveaxis(real, 0, -1)
        real =cv2.cvtColor(real, cv2.COLOR_BGR2RGB)

        print(img_path)

        # compute discriminator loss
        pred_fake = model.netD(visuals['fake_B'])
        loss_D_fake = model.criterionGAN(pred_fake, True).mean()
        pred_real = model.netD(visuals['real_B'])
        loss_D_real = model.criterionGAN(pred_real, True).mean()
        print(f'D (Real must be 1 ) - Fake prediction: {pred_fake.mean()}    Real prediction: {pred_real.mean()} ')

        cv2.imshow("infered", infered)
        cv2.imshow("syn", syn)
        cv2.imshow("real", real)
        cv2.waitKey(0)

        