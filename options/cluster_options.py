def set_config_sincut(opt, job_id):
   
    
    if job_id == 1:
        opt.netG = "stylegan2"
        opt.load_size = 512
        opt.crop_size = 64

    elif job_id == 2:
        opt.netG = "stylegan2"
        opt.load_size = 1080
        opt.crop_size = 64

    # elif job_id == 3:
    #     opt.netG = "stylegan2"
    #     opt.load_size = 512
    #     opt.crop_size = 128

    # elif job_id == 4:
    #     opt.netG = "stylegan2"
    #     opt.load_size = 1080
    #     opt.crop_size = 32

    # elif job_id == 5:
    #     opt.netG = "stylegan2"
    #     opt.load_size = 1080
    #     opt.crop_size = 64

    # elif job_id == 6:
    #     opt.netG = "stylegan2"
    #     opt.load_size = 1080
    #     opt.crop_size = 128

def set_config_cut(opt, job_id):
    """Changes the config based on job id created in the cluster
        To be used for CUT and FasstCUT
    """
    if job_id == 1:
        opt.netG = "resnet_9blocks"
        opt.netD = "basic"

    # elif job_id == 2:
    #     opt.netG = "resnet_6blocks"
    #     opt.netD = "basic"
    
    elif job_id == 2:
        opt.netG = "resnet_4blocks"
        opt.netD = "basic"




    # elif job_id == 4:
    #     opt.netG = "resnet_9blocks"
    #     opt.netD = "n_layers"
    #     opt.n_layers_D = 4
    
    # elif job_id == 5:
    #     opt.netG = "resnet_6blocks"
    #     opt.netD = "n_layers"
    #     opt.n_layers_D = 4
    
    # elif job_id == 6:
    #     opt.netG = "resnet_4blocks"
    #     opt.netD = "n_layers"
    #     opt.n_layers_D = 4

    # elif job_id == 7:
    #     opt.netG = "resnet_9blocks"
    #     opt.netD = "n_layers"
    #     opt.n_layers_D = 2
    
    # elif job_id == 8:
    #     opt.netG = "resnet_6blocks"
    #     opt.netD = "n_layers"
    #     opt.n_layers_D = 2
    
    # elif job_id == 9:
    #     opt.netG = "resnet_4blocks"
    #     opt.netD = "n_layers"
    #     opt.n_layers_D = 2

    # elif job_id == 10:
    #     opt.netG = "resnet_cat"
    #     opt.netD = "basic"

    # elif job_id == 11:
    #     opt.netG = "resnet_cat"
    #     opt.netD = "n_layers"
    #     opt.n_layers_D = 4

    # elif job_id == 12:
    #     opt.netG = "resnet_cat"
    #     opt.netD = "n_layers"
    #     opt.n_layers_D = 2


    