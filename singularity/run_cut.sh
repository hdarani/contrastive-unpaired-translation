#!/bin/bash
#SBATCH --job-name=style_transfer
#SBATCH --mail-type=END,FAIL            # Mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mail-user=hossein.darani@sanctuary.ai   # Where to send mail
#SBATCH --output=/share/storage/home/hossein_darani/contrastive-unpaired-translation/log/style_transfer_ddp_unpaired_bb_cut_%A-%a.txt

#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH -c 32 #48
#SBATCH --gres=gpu:2
# #SBATCH --time=96:00:00
# #SBATCH --begin=now+9hour
# #SBATCH --array=1-4 #24 jobs for cut and fast cut - 8 jobs for sincut

# What to change? --output and --model sincut and array size

if [ $SLURM_ARRAY_TASK_ID == 1 ]; then
        chmod 777 /share/storage/home/hossein_darani/contrastive-unpaired-translation/log
        chmod 777 /share/storage/home/hossein_darani/contrastive-unpaired-translation/wandb
        chmod 777 /share/storage/home/hossein_darani/contrastive-unpaired-translation/checkpoints
fi

# TODO It should not be pushed in git - must be private
export WANDB_API_KEY=6a2823d4cebeec17e844071b9e4b2edba52ae8fa
PORT=$(($RANDOM % 15020 + 15000))


srun singularity exec --nv cut_docker.sif \
		/env/bin/python -m torch.distributed.run --master_port $PORT --standalone --nproc_per_node 2 \
	       	/share/storage/home/hossein_darani/contrastive-unpaired-translation/train.py \
	     	--dataroot ./datasets/unpaired_bb \
	      	--name style_transfer_cut_unpairedbb_ddp \
	       	--model cut \

