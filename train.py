import os
from math import floor
import time
import torch
from options.train_options import TrainOptions
from data import create_dataset
from models import create_model
from util.logger_wb import Logger_wb
import warnings
import random
from util.fid_score import InceptionV3, calculate_fretchet
from torchvision import transforms

from contextlib import contextmanager
import torch.distributed as dist
from torch.nn.parallel import DistributedDataParallel as DDP


LOCAL_RANK = int(os.getenv('LOCAL_RANK', -1))  # https://pytorch.org/docs/stable/elastic/run.html
RANK = int(os.getenv('RANK', -1))
WORLD_SIZE = int(os.getenv('WORLD_SIZE', 1))


def train(model, dataset_training, dataset_eval, inception_model, opt, logger, device):
    total_iters = 0                # the total number of training iterations
    optimize_time = 0.1
    dataset_size = len(dataset_training)    # get the number of images in the dataset.

    for  data in dataset_training:
        model.data_dependent_initialize(data, WORLD_SIZE, device)
        model.setup(opt)               # regular setup: load and print networks; create schedulers
        break
    
    model._ddp(device, WORLD_SIZE, opt)
    
    for epoch in range(opt.epoch_count, opt.n_epochs + opt.n_epochs_decay + 1):    # outer loop for different epochs; we save the model by <epoch_count>, <epoch_count>+<save_latest_freq>
        epoch_start_time = time.time()  # timer for entire epoch
        iter_data_time = time.time()    # timer for data loading per iteration
        epoch_iter = 0                  # the number of training iterations in current epoch, reset to 0 every epoch

        dataset_training.set_epoch(epoch)
        for i, data in enumerate(dataset_training):  # inner loop within one epoch
            iter_start_time = time.time()  # timer for computation per iteration
            if total_iters % opt.print_freq == 0:
                t_data = iter_start_time - iter_data_time

            batch_size = opt.batch_size* WORLD_SIZE
            total_iters += batch_size
            epoch_iter += batch_size
            
            optimize_start_time = time.time()
            

            model.set_input(data, device)  # unpack data from dataset and apply preprocessing
            model.optimize_parameters()   # calculate loss functions, get gradients, update network weights
            
            if RANK in [-1, 0]:
                optimize_time = (time.time() - optimize_start_time) / batch_size * 0.005 + 0.995 * optimize_time

                if total_iters % opt.display_freq == 0:   # display images on visdom and save images to a HTML file
                    eval(model, dataset_eval, inception_model, opt, logger, epoch, epoch_iter, device)

                iter_data_time = time.time()
                if total_iters % opt.print_freq == 0:    # print training losses and save logging information to the disk
                    losses = model.get_current_losses()
                    if logger is not None: logger.print_current_losses(epoch, epoch_iter, losses, optimize_time, t_data)
                    if opt.display_id is None or opt.display_id > 0:
                        if logger is not None: logger.plot_current_losses(epoch, float(epoch_iter) / dataset_size, losses)

                if total_iters % opt.save_latest_freq == 0:   # cache our latest model every <save_latest_freq> iterations
                    print('saving the latest model (epoch %d, total_iters %d)' % (epoch, total_iters))
                    print(opt.name)  # it's useful to occasionally show the experiment name on console
                    save_suffix = 'iter_%d' % total_iters if opt.save_by_iter else 'latest'
                    model.save_networks(save_suffix)
                

        if RANK in [-1, 0]:
            if epoch % opt.save_epoch_freq == 0:      # cache our model every <save_epoch_freq> epochs
                print('saving the model at the end of epoch %d, iters %d' % (epoch, total_iters))
                model.save_networks('latest')
                model.save_networks(epoch)

            print('End of epoch %d / %d \t Time Taken: %d sec' % (epoch, opt.n_epochs + opt.n_epochs_decay, time.time() - epoch_start_time))
            model.update_learning_rate() # update learning rates at the end of every epoch.
                             
def eval(model, dataset_eval, inception_model, opt, logger, epoch, epoch_iter, device):
    opt.phase = "eval"
    model.eval()
    total_visuals = []
    
    num_tests = 1
    if (epoch % opt.save_epoch_freq == 0 or epoch == 1) and epoch_iter / opt.display_freq < 2: # once every opt.save_epoch_freq epoch 
        num_tests = opt.num_test
    
    for i, data in enumerate(dataset_eval):       
        if i >= num_tests:  # only apply our model to opt.num_test images.
            break
        model.set_input(data, device)  # unpack data from data loader
        model.test()           # run inference
        visuals = model.get_current_visuals()  # get image results
        total_visuals.append(visuals)
    model.train()
    opt.phase = "train"

    # calculate MSE for real_b and fake_b (Note data set is paired)
    eval_log = {}
    if opt.input_nc == 1:
        normalizer = transforms.Compose([transforms.Normalize((0.5,), (0.5,))])
    else: 
        normalizer = transforms.Compose([transforms.Normalize((0.0,0.0,0.0), (1.0,1.0,1.0))])
    if opt.input_nc == 1:
        images_real = normalizer(torch.squeeze(torch.cat( [visuals["real_B"].repeat(1,3,1,1) for visuals in total_visuals], 0)))
        images_fake = normalizer(torch.squeeze(torch.cat( [visuals['fake_B'].repeat(1,3,1,1) for visuals in total_visuals], 0)))
    else:
        images_real = normalizer(torch.squeeze(torch.cat( [visuals["real_B"] for visuals in total_visuals], 0)))
        images_fake = normalizer(torch.squeeze(torch.cat( [visuals['fake_B'] for visuals in total_visuals], 0))) 


    batch = 128
    fid_sum = 0
    num_truncated_batches = floor(opt.num_test/batch)
    if (epoch % opt.save_epoch_freq == 0 or epoch == 1) and epoch_iter / opt.display_freq < 2: # once every opt.save_epoch_freq epoch
        for i in range(num_truncated_batches):
            fid_sum += calculate_fretchet(images_real[i*batch : (i+1)*batch-1, ...] , images_fake[i*batch : (i+1)*batch-1, ...], inception_model)

        eval_log['FID'] = fid_sum/num_truncated_batches
    
    if logger is not None:
        if 'FID' in eval_log.keys():
            logger.plot_current_eval(eval_log)
        logger.display_current_results(random.choice(total_visuals))



def main(opt):

    #DDP mode
    device = torch.device('cuda:{}'.format(opt.gpu_ids[0])) if opt.gpu_ids else torch.device('cpu')
    if LOCAL_RANK !=-1:
        assert torch.cuda.device_count() > LOCAL_RANK, 'insufficient CUDA devices for DDP command'
        torch.cuda.set_device(LOCAL_RANK)
        device = torch.device('cuda', LOCAL_RANK)
        dist.init_process_group(backend="nccl")


    log = False
    # create a train dataset given opt.dataset_mode and other options
    opt.phase = 'train'
    dataset_training = create_dataset(opt, WORLD_SIZE)
    model = create_model(opt)   # create a model given opt.model and other options

    if RANK in [-1, 0]:
        print('The number of training images = %d' % len(dataset_training))
        print("model [%s] was created" % type(model).__name__)

    # create a validation dataset given opt.dataset_mode and other options
    dataset_eval = None
    inception_model = None
    if RANK in [-1, 0]:
        if opt.eval:
            opt.phase = 'eval'
            dataset_eval = create_dataset(opt, WORLD_SIZE)      # create a dataset for evaluations
            opt.num_test = len(dataset_eval)
            opt.phase = 'train'
            if RANK in [-1, 0]:
                print('The number of eval images = %d' % len(dataset_eval))
            
            # initiate  inception for calculating FID score
            block_idx = InceptionV3.BLOCK_INDEX_BY_DIM[2048]
            inception_model = InceptionV3([block_idx])
            inception_model=inception_model.to(device)

   
    logger = None
    if RANK in [-1, 0]:
        if log:
            logger = Logger_wb(opt) # create the wandb logger
        else:
            warnings.warn("Logger is of")
            print("-"*50)
            print("-"*50)
            print("------------------- LOGGING IS OFF --------------------")
            print("-"*50)
            print("-"*50)
    
        
        
    train(model, dataset_training, dataset_eval, inception_model, opt, logger, device)

    if WORLD_SIZE > 1 and RANK == 0:
        print('Destroying process group... ')
        dist.destroy_process_group()
    if RANK in [-1, 0]:
        if log:
            logger.finish()


if __name__ == '__main__':

    opt = TrainOptions().parse()

    # Modify opts before creating dataset and model    
    opt.load_size = 128
    opt.load_size_test = opt.load_size
    opt.serial_batches = True
    opt.batch_size = opt.batch_size * WORLD_SIZE
    
    opt.name = opt.name + "_" + opt.CUT_mode + "_" + opt.model+  "_" + opt.netG + "_" + opt.netD + "_" + str(opt.n_layers_D) + "_" + str(opt.load_size) +"_" + str(opt.crop_size)+ "_" + opt.preprocess + "_" + str(opt.lambda_L1)
        
    main(opt)
