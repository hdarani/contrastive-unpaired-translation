from options.test_options import TestOptions
from models import create_model
import numpy as np
import os
import json
import cv2
from pathlib import Path
import torchvision.transforms as transforms
import torch
import argparse
import time
import shutil
"""
This utility script implementes Algorithm 1: Inference flow for a single image
in https://arxiv.org/pdf/2203.02069.pdf
"""
"""
"args": [ 
                "--name", "style_transfer_blue_bin_paired",
                "--CUT_mode", "cut",
                "--eval", "True",
                "--checkpoints_dir_test","/home/HosseinDarani/CUT/checkpoints/paired_blue_bin/style_transfer_1_cut_cut_resnet_9blocks_n_layers_2_128_128_resize",
                "--epoch_test", "140",
                "--n_downsampling", "3",
                "--dataroot", "/home/HosseinDarani/scene_generation/examples/output_images/blu_bin_results/crop_data"
            ],
"""

total_obj = 0
filtered_out = 0
total_D_pred = []
class Inference():
    def __init__(self, opt) -> None:
        
        self.resize_size = opt.resize
        self.num_files = opt.num_files
        self.parent_dir = opt.img_tes_dir
        self.image_dir = os.path.join(self.parent_dir , '/image_data/')
        self.depth_dir = os.path.join(self.parent_dir , '/depth_data/')
        self.save_dir = os.path.join( self.parent_dir , '/style_transfered/')
        Path(self.save_dir).mkdir(parents=True, exist_ok=True)

        self.discriminator_filter = opt.discriminator_fiter
        self.thr = opt.thresh        
        self.show = opt.show
        self.save = opt.save
        self.model_initialized = False

        self.opt = opt
        self.opt.gan_mode ='lsgan'

        self.transform = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

        self.models = []
        for i, dirs in enumerate(self.opt.checkpoints_dir_test[0].split(',')): # if you want Ensemble
            self.opt.checkpoints_dir = dirs.strip()
            self.opt.epoch = self.opt.epoch_test[0].split(',')[i]
            self.models.append(create_model(self.opt))      # create a model given opt.model and other options

        
    def run(self):

        filenames = [f'img_{i:06d}' for i in range(self.num_files)]
        data = {}
        data['A_paths'] = ""
        data['B_paths'] = ""
        global total_obj
        global total_D_pred
        start_time = time.time()

        for i,  filename in enumerate(filenames):
            if i%1000==0:
                postfix = f'{int(i/1000):03d}'
                Path(self.save_dir+postfix).mkdir(parents=True, exist_ok=True)

            # crop the image
            cropped_images , img, gt = self.crop(self.image_dir+ postfix, self.depth_dir+postfix, filename)
            # load mask crop
            cropped_masks, _, _ = self.crop(self.depth_dir+postfix, self.depth_dir+postfix, filename, use_png=True)
            
            infered_images = []
            discriminator_score = []
            
            
            for img_crop in cropped_images:
                if img_crop is None:
                    infered_images.append(None)
                    discriminator_score.append(-1)
                    continue

                total_obj+=1
                # resize the image
                img_crop_resized = cv2.resize(img_crop, (self.resize_size, self.resize_size), interpolation = cv2.INTER_CUBIC)
                if self.show:
                    cv2.imshow("original", img)
                    cv2.imshow("original crop", img_crop_resized)
                    
                # Inference
                crop_tensor = self.transform(img_crop_resized)            
                data['A'] = torch.unsqueeze(crop_tensor,0)
                data['B'] = torch.unsqueeze(crop_tensor,0)

                if not self.model_initialized:
                    for model in self.models:
                        model.data_dependent_initialize(data)
                        model.setup(self.opt)               # regular setup: load and print networks; create schedulers
                        model.eval()
                    self.model_initialized = True
    
                infereds_resized = []
                for model in self.models:
                    model.set_input(data)  # unpack data from data loader
                    model.test()           # run inference
                    visuals = model.get_current_visuals()  # get image results

                    infered = visuals['fake_B'].detach().cpu().numpy().squeeze()
                    infered = np.moveaxis(infered, 0, -1)
                    infered =cv2.cvtColor(infered, cv2.COLOR_BGR2RGB)
                    infereds_resized.append(cv2.resize(infered, (img_crop.shape[1], img_crop.shape[0]), interpolation = cv2.INTER_AREA))

                 # compute discriminator loss
                pred_fake = model.netD(visuals['fake_B'])
                discriminator_score.append(pred_fake.mean())
                total_D_pred.append(pred_fake.mean().detach().cpu().numpy())

                infered_resized = np.zeros_like(infereds_resized[0])
                for infered in infereds_resized:
                    infered_resized += infered
                infered_resized = cv2.normalize(infered_resized, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)
                infered_images.append(infered_resized)
                
                if self.show:
                    cv2.imshow("infered cropped", infered_resized)
            
            overlayed_img = self.overlay(cropped_masks, infered_images, cropped_images, discriminator_score, img.copy(), gt)
            
            if self.show:
                cv2.imshow("transfered", overlayed_img)
                cv2.waitKey(0)
            if self.save:
                cv2.imwrite(os.path.join(self.save_dir+postfix, filename+ '.jpg'), overlayed_img)
                shutil.copy(os.path.join(self.image_dir+ postfix, filename+'.txt'), os.path.join(self.save_dir+postfix, filename+ '.txt'))

        print(f'Total number of objects: {total_obj} - Rejected by discriminator: {filtered_out}')
        print(f'Total time: {time.time() - start_time}')
        print(f'Discriminator avg: {np.mean(np.asarray(total_D_pred))}')

    def crop(self, image_dir, depth_dir, filename, show_img = False, use_png = False):

        if use_png:
            img_file = os.path.join(image_dir, filename + '_mask.png')
        else:
            img_file = os.path.join(image_dir, filename+'.jpg')
        img = cv2.imread(img_file)
        orig_img = img
        gt_file = os.path.join(depth_dir, filename+'_gt.json')
        with open(gt_file, 'r') as json_file:
            gt = json.load(json_file)

        
        cropped_imgs = []
        all_objects = gt['objects']
        for object_index, scene_object in enumerate(all_objects):
            cropped_img = None
            # draw 2d-bounding box:
            if scene_object['name'] != "blue_bin":
                continue
            bb = scene_object['2d_bounding_box']
            delta_x = bb["max_x"] - bb["min_x"]
            scaled_delta_x = 1.2 * delta_x
            diff_x = int((scaled_delta_x - delta_x)/2)
            bb["min_x"] -= diff_x



            bb["max_x"] += diff_x

            delta_y = bb["max_y"] - bb["min_y"]
            scaled_delta_y = 1.2 * delta_y
            diff_y = int((scaled_delta_y - delta_y)/2)
            bb["min_y"] -= diff_y
            bb["max_y"] += diff_y
            if bb["min_x"]>0 and bb["min_y"]>0 and bb["max_y"]<img.shape[0] and bb["max_x"]<img.shape[1]:
                cropped_img = img[bb["min_y"] : bb["max_y"]+1 , bb["min_x"] : bb["max_x"]+1 ]
                if not use_png: # do not cvt color the mask
                    cropped_img =cv2.cvtColor(cropped_img, cv2.COLOR_RGB2BGR)
                cropped_imgs.append(cropped_img)
            else:
                cropped_imgs.append(cropped_img)

        if show_img:
            cv2.imshow('image', img)
            cv2.waitKey(0)
        
        return cropped_imgs, orig_img, gt


    def overlay(self, cropped_masks, infered_images, cropped_images, discriminator_score, img, gt):
        global filtered_out
        all_objects = gt['objects']
        masks = []
        for i, scene_object in enumerate(all_objects):
                if scene_object['name'] == "blue_bin":
                    masks.append(i+1)

        for indx, (mask_crop, infered_crop, img_crop, d_score) in enumerate(zip(cropped_masks, infered_images, cropped_images , discriminator_score)):
            if img_crop is None:
                continue
            if d_score < self.thr:
                filtered_out += 1
                continue
            img_crop =cv2.cvtColor(img_crop, cv2.COLOR_BGR2RGB)
            
            mask_crop = np.where(mask_crop==masks[indx] , 1, 0) #TODO how to find seg id of ooi
            
            overlay_object = np.multiply(mask_crop, infered_crop)
            overlay_background = np.multiply( (1-mask_crop) , img_crop)
           
            overlay_crop = overlay_background + overlay_object
            
            ooi_indx = -1
            for scene_object in all_objects:
                if scene_object['name'] == "blue_bin":
                    ooi_indx+=1
                    if ooi_indx == indx:
                        # draw 2d-bounding box:
                        bb = scene_object['2d_bounding_box']
                        
                        if bb["min_x"]>0 and bb["min_y"]>0 and bb["max_y"]<img.shape[0] and bb["max_x"]<img.shape[1]:
                            img[bb["min_y"] : bb["max_y"]+1 , bb["min_x"] : bb["max_x"]+1 ] = overlay_crop
                        else:
                            raise Exception("Overlay Error - Bad GT")
        return img

if __name__ == "__main__":
    opt = TestOptions().parse()  # get test options

    infernce_flow = Inference(opt)
    infernce_flow.run()